package service

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"template/db"
	"template/model"
)

// SaveUser - save user to database
func SaveUser(body io.ReadCloser) {
	b, err := ioutil.ReadAll(body)
	var user model.User
	if err != nil {
		log.Fatal(err)
	}
	err = json.Unmarshal(b, &user)
	tx := db.Db.Begin()
	err = tx.Save(user).Error
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}
	tx.Commit()
	err = tx.Close()
	if err != nil {
		log.Fatal(err)
	}
}
