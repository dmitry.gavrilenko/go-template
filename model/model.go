package model

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	name     string
	email    string `gorm:"unique"`
	password string
}
