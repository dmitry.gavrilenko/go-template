package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_"github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"time"
)

var Db *gorm.DB

const(
	DIALECT = "postgres"
	URL = "user=root password=root dbname=template sslmode=disable"
)

func InitDatabase() {
	var err error
	Db, err = gorm.Open(DIALECT, URL)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(time.Now().Format(time.ANSIC), "Database started")
}