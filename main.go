package main

import (
	"template/db"
	"template/route"
)

func main() {
	db.InitDatabase()
	route.InitServer()
}
