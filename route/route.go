package route

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"template/service"
	"time"
)

// Save - user save handler
func Save(w http.ResponseWriter, r *http.Request) {
	service.SaveUser(r.Body)
	fmt.Fprintf(w, "User saved successfully")
}

func Greeting(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello")
}


func InitServer(){
	r := mux.NewRouter().PathPrefix("/api/v1/").Subrouter()
	r.HandleFunc("/user/save", Save)
	r.HandleFunc("/greeting", Greeting)
	fmt.Println(time.Now().Format(time.ANSIC), "Server started")
	fmt.Println(time.Now().Format(time.ANSIC), "Listen port: 8080")
	err := http.ListenAndServe(":8080", r)
	if err != nil {
		log.Fatal("Failed start server", err)
	}
}